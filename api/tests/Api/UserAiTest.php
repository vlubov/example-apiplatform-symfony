<?php

namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

class UserAiTest extends ApiTestCase
{
    public function testCreateGreeting()
    {
        static::createClient()->request(
            'POST',
            '/user',
            [
                'json' => []
            ]
        );

        $this->assertResponseStatusCodeSame(201);
    }
}
