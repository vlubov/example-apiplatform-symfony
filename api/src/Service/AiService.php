<?php

declare(strict_types=1);

namespace App\Service;

use App\Document\CategoryInterest;
use App\Document\Pages;
use App\Document\SiteInterest;
use App\Document\SubCategory;
use App\Document\UserAi;
use App\Document\Visitor;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\MongoDBException;
use Symfony\Component\HttpFoundation\Request;

class AiService
{
    private UserAi $user;
    private DocumentManager $documentManager;
    private Request $request;

    public function __construct(
        UserAi $user,
        DocumentManager $documentManager,
        Request $request)
    {
        $this->user = $user;
        $this->documentManager= $documentManager;
        $this->request = $request;
    }

    /**
     * @return UserAi
     * @throws MongoDBException
     */
    public function execute(): UserAi
    {
        $requestData = json_decode($this->request->getContent(), true);

        $url = str_replace('https://www.', '', $requestData['pageURL']);
        $data = explode('/', $url);

        /** В разработке */
        $this->createPagesInterest($requestData['pageInfo']);
        $this->createVisitors($requestData['visitorInfo']);

        $this->user->setSiteInterestId(
            $this->createSiteInterest($data, $requestData['get']['name'])
        );
        $this->user->setCategoryInterestId($this->createCategoryInterest($data));
        $this->user->setSubcategoryInterestId($this->createSubcategory($data));

        $services = $requestData['services'];
        $this->user->setIp($requestData['ip']);
        $this->user->setCookieId($requestData['cookie']);
        $this->user->setBrowser($requestData['browser']);
        $this->user->setGoogleId((int)$services['google']['userID']);
        $this->user->setYandexId((int)$services['yandex']['clientID']);

        $this->documentManager->persist($this->user);

        $this->documentManager->flush();

        return $this->user;
    }

    /**
     * Создает матрицу интересов сайта.
     *
     * @param array $data
     * @return int
     * @throws MongoDBException
     */
    private function createSiteInterest(array $data, string $name): int
    {
        $siteInterest = new SiteInterest();
        $siteInterest->setSites(implode('/', $data));
        $siteInterest->setName($name);
        $siteInterest->setCategory($data[0] ?? '');
        $siteInterest->setSubCategory($data[1] ?? '');

        $this->documentManager->persist($siteInterest);
        $this->documentManager->flush();

        return $siteInterest->getId();
    }

    /**
     * Создает матрицу интересов категории.
     *
     * @param array $data
     * @return int
     * @throws MongoDBException
     */
    private function createCategoryInterest(array $data): int
    {
        $category = new CategoryInterest();
        $category->setName($data[0]);
        $category->setCategory($data[1] ?? '');
        $category->setSubCategory($data[2] ?? '');

        $this->documentManager->persist($category);
        $this->documentManager->flush();

        return $category->getId();
    }

    /**
     * Создает матрицу интересов подкатегории.
     *
     * @param array $data
     * @return int
     * @throws MongoDBException
     */
    private function createSubcategory(array $data): int
    {
        $subCategory = new Subcategory();
        $subCategory->setName($data[0]);
        $subCategory->setCategory($data[1] ?? '');
        $subCategory->setSubCategory($data[2] ?? '');

        $this->documentManager->persist($subCategory);
        $this->documentManager->flush();

        return $subCategory->getId();
    }

    // TODO: Требует обсуждения, ибо данных мало приходит, чтобы собрать такие сущности.
    private function createPagesInterest(array $data)
    {
        $pages = new Pages();
        $pages->setAmountBeginDialogs($data['amountBeginDialogs']);
        $pages->setAmountCalls($data['amountCalls']);
        $pages->setAmountCompletedCalls($data['amountCompletedCalls']);
        $pages->setAmountNotCompletedCalls($data['amountNotCompletedCalls']);
        $pages->setAmountUniqVisit($data['amountUniqVisit']);
        $pages->setAvgTimeScrollAction($data['avgTimeScrollAction']);
        $pages->setAvgTimeVisit($data['avgTimeVisit']);
        $pages->setCalls($data['calls']);
        $pages->setCategory($data['category']);
        $pages->setDialogs($data['dialogs']);
        $pages->setInteractions($data['interactions']);
        $pages->setMaxTimeScrollAction($data['maxTimeScrollAction']);
        $pages->setMaxTimeVisit($data['maxTimeVisit']);
        $pages->setMinTimeScrollAction($data['minTimeScrollAction']);
        $pages->setMinTimeVisit($data['minTimeVisit']);
        $pages->setRateRejection($data['rateRejection']);
        $pages->setSite($data['site']);
        $pages->setSubCategory($data['subCategory']);
        $pages->setUrl($data['url']);

        $this->documentManager->persist($pages);
        $this->documentManager->flush();

        return $pages->getId();
    }

    private function createVisitors(array $data)
    {
        $visitor = new Visitor();
        $visitor->setAvgTimeScrollAction($data['avgTimeScrollAction']);
        $visitor->setAmountNotCompletedCalls($data['amountNotCompletedCalls']);
        $visitor->setAmountCompletedCalls($data['aountCompletedCalls']);
        $visitor->setAmountCalls($data['amountCalls']);
        $visitor->setAvgVisitTime($data['avgVisitTime']);
        $visitor->setCountVisit($data['countVisit']);
        $visitor->setCountVisitPages($data['countVisitPages']);
        $visitor->setDeviceFirstVisit($data['deviceFirstVisit']);
        $visitor->setDeviceLastVisit($data['deviceLastVisit']);
        $visitor->setDialogs($data['dialogs']);
        $visitor->setFirstTrafficSource($data['firstTrafficSource']);
        $visitor->setFirstVisit($data['firstVisit']);
        $visitor->setLastIpAddress($data['lastIpAddress']);
        $visitor->setLastTrafficSource($data['lastTrafficSource']);
        $visitor->setLastVisit($data['lastVisit']);
        $visitor->setLosedTraffic($data['losedTraffic']);
        $visitor->setOSFirstVisit($data['osFirstVisit']);
        $visitor->setRegionTrafficSource($data['regionTrafficSource']);
        $visitor->setVisitTime($data['visitTime']);

        $this->documentManager->persist($visitor);
        $this->documentManager->flush();

        return $visitor->getId();
    }
}
