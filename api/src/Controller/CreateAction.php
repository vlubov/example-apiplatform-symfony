<?php

declare(strict_types=1);

namespace App\Controller;

use App\Document\UserAi;
use App\Service\AiService;

/**
 * Class CreateAction
 */
class CreateAction
{
    private AiService $aiService;

    public function __construct(AiService $aiService)
    {

        $this->aiService = $aiService;
    }

    public function __invoke(): UserAi
    {
        return $this->aiService->execute();
    }
}
