<?php

namespace App\Repository;

use App\Document\CategoryInterest;
use Doctrine\Bundle\MongoDBBundle\Repository\ServiceDocumentRepository;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;

/**
 * @method CategoryInterest|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoryInterest|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoryInterest[]    findAll()
 * @method CategoryInterest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryInterestRepository extends ServiceDocumentRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategoryInterest::class);
    }

    // /**
    //  * @return CategoryInterest[] Returns an array of CategoryInterest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategoryInterest
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
