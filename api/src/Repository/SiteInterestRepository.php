<?php

namespace App\Repository;

use App\Document\SiteInterest;
use Doctrine\Bundle\MongoDBBundle\Repository\ServiceDocumentRepository;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;

/**
 * @method SiteInterest|null find($id, $lockMode = null, $lockVersion = null)
 * @method SiteInterest|null findOneBy(array $criteria, array $orderBy = null)
 * @method SiteInterest[]    findAll()
 * @method SiteInterest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SiteInterestRepository extends ServiceDocumentRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SiteInterest::class);
    }

    // /**
    //  * @return SiteInterest[] Returns an array of SiteInterest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SiteInterest
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
