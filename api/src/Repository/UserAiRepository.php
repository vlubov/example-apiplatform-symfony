<?php

namespace App\Repository;

use App\Document\UserAi;
use Doctrine\Bundle\MongoDBBundle\Repository\ServiceDocumentRepository;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;

/**
 * @method UserAi|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserAi|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserAi[]    findAll()
 * @method UserAi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserAiRepository extends ServiceDocumentRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserAi::class);
    }

    // /**
    //  * @return UserAi[] Returns an array of UserAi objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserAi
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
