<?php

namespace App\Document;

use App\Repository\SubCategoryRepository;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(repositoryClass=SubCategoryRepository::class)
 */
class Visitor
{
    /**
     * @ODM\Id(strategy="INCREMENT", type="int")
     */
    private $id;

    /**
     * @ODM\Field(type="int")
     */
    private $countVisit;

    /**
     * @ODM\Field(type="collection")
     */
    private $visitTime;

    /**
     * @ODM\Field(type="collection")
     */
    private $avgVisitTime;

    /**
     * @ODM\Field(type="collection")
     */
    private $countVisitPages;

    /**
     * @ODM\Field(type="collection")
     */
    private $minTimeScrollAction;

    /**
     * @ODM\Field(type="collection")
     */
    private $avgTimeScrollAction;

    /**
     * @ODM\Field(type="collection")
     */
    private $maxTimeScrollAction;

    /**
     * @ODM\Field(type="collection")
     */
    private $firstVisit;

    /**
     * @ODM\Field(type="collection")
     */
    private $firstTrafficSource;

    /**
     * @ODM\Field(type="collection")
     */
    private $regionTrafficSource;

    /**
     * @ODM\Field(type="collection")
     */
    private $deviceFirstVisit;

    /**
     * @ODM\Field(type="collection")
     */
    private $osFirstVisit;

    /**
     * @ODM\Field(type="collection")
     */
    private $lastVisit;

    /**
     * @ODM\Field(type="collection")
     */
    private $lastTrafficSource;

    /**
     * @ODM\Field(type="collection")
     */
    private $deviceLastVisit;

    /**
     * @ODM\Field(type="collection")
     */
    private $lastIpAddress;

    /**
     * Отказность.
     *
     * @ODM\Field(type="collection")
     */
    private $losedTraffic;

    /**
     * Кол-во звонков.
     *
     * @ODM\Field(type="collection")
     */
    private $amountCalls;

    /**
     * Количество завершенных диалогов.
     *
     * @ODM\Field(type="collection")
     */
    private $amountCompletedCalls;

    /**
     * Количество незавершенных диалогов.
     *
     * @ODM\Field(type="collection")
     */
    private $amountNotCompletedCalls;

    /**
     * Диалоги.
     *
     * @ODM\Field(type="collection")
     */
    private $dialogs;

    /**
     * Взаимодействия.
     *
     * @ODM\Field(type="collection")
     */
    private $interactions;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCountVisit()
    {
        return $this->countVisit;
    }

    /**
     * @param mixed $countVisit
     */
    public function setCountVisit($countVisit): void
    {
        $this->countVisit = $countVisit;
    }

    /**
     * @return mixed
     */
    public function getVisitTime()
    {
        return $this->visitTime;
    }

    /**
     * @param mixed $visitTime
     */
    public function setVisitTime($visitTime): void
    {
        $this->visitTime = $visitTime;
    }

    /**
     * @return mixed
     */
    public function getAvgVisitTime()
    {
        return $this->avgVisitTime;
    }

    /**
     * @param mixed $avgVisitTime
     */
    public function setAvgVisitTime($avgVisitTime): void
    {
        $this->avgVisitTime = $avgVisitTime;
    }

    /**
     * @return mixed
     */
    public function getCountVisitPages()
    {
        return $this->countVisitPages;
    }

    /**
     * @param mixed $countVisitPages
     */
    public function setCountVisitPages($countVisitPages): void
    {
        $this->countVisitPages = $countVisitPages;
    }

    /**
     * @return mixed
     */
    public function getMinTimeScrollAction()
    {
        return $this->minTimeScrollAction;
    }

    /**
     * @param mixed $minTimeScrollAction
     */
    public function setMinTimeScrollAction($minTimeScrollAction): void
    {
        $this->minTimeScrollAction = $minTimeScrollAction;
    }

    /**
     * @return mixed
     */
    public function getAvgTimeScrollAction()
    {
        return $this->avgTimeScrollAction;
    }

    /**
     * @param mixed $avgTimeScrollAction
     */
    public function setAvgTimeScrollAction($avgTimeScrollAction): void
    {
        $this->avgTimeScrollAction = $avgTimeScrollAction;
    }

    /**
     * @return mixed
     */
    public function getMaxTimeScrollAction()
    {
        return $this->maxTimeScrollAction;
    }

    /**
     * @param mixed $maxTimeScrollAction
     */
    public function setMaxTimeScrollAction($maxTimeScrollAction): void
    {
        $this->maxTimeScrollAction = $maxTimeScrollAction;
    }

    /**
     * @return mixed
     */
    public function getFirstVisit()
    {
        return $this->firstVisit;
    }

    /**
     * @param mixed $firstVisit
     */
    public function setFirstVisit($firstVisit): void
    {
        $this->firstVisit = $firstVisit;
    }

    /**
     * @return mixed
     */
    public function getFirstTrafficSource()
    {
        return $this->firstTrafficSource;
    }

    /**
     * @param mixed $firstTrafficSource
     */
    public function setFirstTrafficSource($firstTrafficSource): void
    {
        $this->firstTrafficSource = $firstTrafficSource;
    }

    /**
     * @return mixed
     */
    public function getRegionTrafficSource()
    {
        return $this->regionTrafficSource;
    }

    /**
     * @param mixed $regionTrafficSource
     */
    public function setRegionTrafficSource($regionTrafficSource): void
    {
        $this->regionTrafficSource = $regionTrafficSource;
    }

    /**
     * @return mixed
     */
    public function getDeviceFirstVisit()
    {
        return $this->deviceFirstVisit;
    }

    /**
     * @param mixed $deviceFirstVisit
     */
    public function setDeviceFirstVisit($deviceFirstVisit): void
    {
        $this->deviceFirstVisit = $deviceFirstVisit;
    }

    /**
     * @return mixed
     */
    public function getOSFirstVisit()
    {
        return $this->osFirstVisit;
    }

    /**
     * @param mixed $osFirstVisit
     */
    public function setOSFirstVisit($osFirstVisit): void
    {
        $this->osFirstVisit = $osFirstVisit;
    }

    /**
     * @return mixed
     */
    public function getLastVisit()
    {
        return $this->lastVisit;
    }

    /**
     * @param mixed $lastVisit
     */
    public function setLastVisit($lastVisit): void
    {
        $this->lastVisit = $lastVisit;
    }

    /**
     * @return mixed
     */
    public function getLastTrafficSource()
    {
        return $this->lastTrafficSource;
    }

    /**
     * @param mixed $lastTrafficSource
     */
    public function setLastTrafficSource($lastTrafficSource): void
    {
        $this->lastTrafficSource = $lastTrafficSource;
    }

    /**
     * @return mixed
     */
    public function getDeviceLastVisit()
    {
        return $this->deviceLastVisit;
    }

    /**
     * @param mixed $deviceLastVisit
     */
    public function setDeviceLastVisit($deviceLastVisit): void
    {
        $this->deviceLastVisit = $deviceLastVisit;
    }

    /**
     * @return mixed
     */
    public function getLastIpAddress()
    {
        return $this->lastIpAddress;
    }

    /**
     * @param mixed $lastIpAddress
     */
    public function setLastIpAddress($lastIpAddress): void
    {
        $this->lastIpAddress = $lastIpAddress;
    }

    /**
     * @return mixed
     */
    public function getLosedTraffic()
    {
        return $this->losedTraffic;
    }

    /**
     * @param mixed $losedTraffic
     */
    public function setLosedTraffic($losedTraffic): void
    {
        $this->losedTraffic = $losedTraffic;
    }

    /**
     * @return mixed
     */
    public function getAmountCalls()
    {
        return $this->amountCalls;
    }

    /**
     * @param mixed $amountCalls
     */
    public function setAmountCalls($amountCalls): void
    {
        $this->amountCalls = $amountCalls;
    }

    /**
     * @return mixed
     */
    public function getAmountCompletedCalls()
    {
        return $this->amountCompletedCalls;
    }

    /**
     * @param mixed $amountCompletedCalls
     */
    public function setAmountCompletedCalls($amountCompletedCalls): void
    {
        $this->amountCompletedCalls = $amountCompletedCalls;
    }

    /**
     * @return mixed
     */
    public function getAmountNotCompletedCalls()
    {
        return $this->amountNotCompletedCalls;
    }

    /**
     * @param mixed $amountNotCompletedCalls
     */
    public function setAmountNotCompletedCalls($amountNotCompletedCalls): void
    {
        $this->amountNotCompletedCalls = $amountNotCompletedCalls;
    }

    /**
     * @return mixed
     */
    public function getDialogs()
    {
        return $this->dialogs;
    }

    /**
     * @param mixed $dialogs
     */
    public function setDialogs($dialogs): void
    {
        $this->dialogs = $dialogs;
    }

    /**
     * @return mixed
     */
    public function getInteractions()
    {
        return $this->interactions;
    }

    /**
     * @param mixed $interactions
     */
    public function setInteractions($interactions): void
    {
        $this->interactions = $interactions;
    }
}
