<?php

namespace App\Document;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\CreateAction;
use App\Repository\UserAiRepository;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(repositoryClass=UserAiRepository::class)
 */
#[ApiResource(collectionOperations: [
    'get',
    'post' => [
        'method' => 'POST',
        'path' => '/user',
        'controller' => CreateAction::class,
        'input_formats' => ['json' => [ 'application/json']],
       // 'output_formats' => ['json' => [ 'application/json']],
        'swagger_context' => [
            'parameters' => []
        ]
    ],

    ],
    formats: ['json']
)]
class UserAi
{
    /**
     * @ODM\Id(strategy="INCREMENT", type="int")
     */
    private int $id;

    /**
     * @ODM\Field(type="integer", nullable=true)
     */
    private int $siteInterestId;

    /**
     * @ODM\Field(type="integer", nullable=true)
     */
    private int $subcategoryInterestId;

    /**
     * @ODM\Field(type="integer", nullable=true)
     */
    private int $categoryInterestId;

    /**
     * @ODM\Field(type="string")
     */
    private string $cookieId;

    /**
     * @ODM\Field(type="integer", nullable=true)
     */
    private int $yandexId;

    /**
     * @ODM\Field(type="integer", nullable=true)
     */
    private int $googleId;

    /**
     * @ODM\Field(type="string")
     */
    private string $ip;

    /**
     * @ODM\Field(type="string", nullable=true)
     */
    private string $browser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSiteInterestId(): ?int
    {
        return $this->siteInterestId;
    }

    public function setSiteInterestId(?int $siteInterestId): self
    {
        $this->siteInterestId = $siteInterestId;

        return $this;
    }

    public function getSubcategoryInterestId(): ?int
    {
        return $this->subcategoryInterestId;
    }

    public function setSubcategoryInterestId(?int $subcategoryInterestId): self
    {
        $this->subcategoryInterestId = $subcategoryInterestId;

        return $this;
    }

    public function getCategoryInterestId(): ?int
    {
        return $this->categoryInterestId;
    }

    public function setCategoryInterestId(?int $categoryInterestId): self
    {
        $this->categoryInterestId = $categoryInterestId;

        return $this;
    }

    public function getCookieId(): ?string
    {
        return $this->cookieId;
    }

    public function setCookieId(string $cookieId): self
    {
        $this->cookieId = $cookieId;

        return $this;
    }

    public function getYandexId(): ?int
    {
        return $this->yandexId;
    }

    public function setYandexId(?int $yandexId): self
    {
        $this->yandexId = $yandexId;

        return $this;
    }

    public function getGoogleId(): ?int
    {
        return $this->googleId;
    }

    public function setGoogleId(?int $googleId): self
    {
        $this->googleId = $googleId;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getBrowser(): ?string
    {
        return $this->browser;
    }

    public function setBrowser(?string $browser): self
    {
        $this->browser = $browser;

        return $this;
    }
}
