<?php

namespace App\Document;

use App\Repository\SiteInterestRepository;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(repositoryClass=SiteInterestRepository::class)
 */
class Pages
{
    /**
     * @ODM\Id(strategy="INCREMENT", type="int")
     */
    private $id;

    /**
     * @ODM\Id(type="string")
     */
    private $url;

    /**
     * @ODM\Id(type="string")
     */
    private $site;

    /**
     * @ODM\Id(type="string")
     */
    private $category;

    /**
     * @ODM\Id(type="string")
     */
    private $subCategory;

    /**
     * @ODM\Id(type="string")
     */
    private $minTimeVisit;

    /**
     * @ODM\Id(type="string")
     */
    private $avgTimeVisit;

    /**
     * @ODM\Id(type="string")
     */
    private $maxTimeVisit;

    /**
     * @ODM\Field(type="collection")
     */
    private $minTimeScrollAction;

    /**
     * @ODM\Field(type="collection")
     */
    private $avgTimeScrollAction;

    /**
     * @ODM\Field(type="collection")
     */
    private $maxTimeScrollAction;

    /**
     * @ODM\Id(type="string")
     */
    private $amountUniqVisit;

    /**
     * Процент отказов.
     *
     * @ODM\Id(type="string")
     */
    private $rateRejection;

    /**
     * @ODM\Id(type="string")
     */
    private $amountCalls;

    /**
     * @ODM\Id(type="string")
     */
    private $calls;

    /**
     * @ODM\Id(type="string")
     */
    private $amountBeginDialogs;

    /**
     * Количество завершенных диалогов.
     *
     * @ODM\Field(type="collection")
     */
    private $amountCompletedCalls;

    /**
     * Количество незавершенных диалогов.
     *
     * @ODM\Field(type="collection")
     */
    private $amountNotCompletedCalls;

    /**
     * Диалоги.
     *
     * @ODM\Field(type="collection")
     */
    private $dialogs;

    /**
     * Взаимодействия.
     *
     * @ODM\Field(type="collection")
     */
    private $interactions;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param mixed $site
     */
    public function setSite($site): void
    {
        $this->site = $site;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getSubCategory()
    {
        return $this->subCategory;
    }

    /**
     * @param mixed $subCategory
     */
    public function setSubCategory($subCategory): void
    {
        $this->subCategory = $subCategory;
    }

    /**
     * @return mixed
     */
    public function getMinTimeVisit()
    {
        return $this->minTimeVisit;
    }

    /**
     * @param mixed $minTimeVisit
     */
    public function setMinTimeVisit($minTimeVisit): void
    {
        $this->minTimeVisit = $minTimeVisit;
    }

    /**
     * @return mixed
     */
    public function getAvgTimeVisit()
    {
        return $this->avgTimeVisit;
    }

    /**
     * @param mixed $avgTimeVisit
     */
    public function setAvgTimeVisit($avgTimeVisit): void
    {
        $this->avgTimeVisit = $avgTimeVisit;
    }

    /**
     * @return mixed
     */
    public function getMaxTimeVisit()
    {
        return $this->maxTimeVisit;
    }

    /**
     * @param mixed $maxTimeVisit
     */
    public function setMaxTimeVisit($maxTimeVisit): void
    {
        $this->maxTimeVisit = $maxTimeVisit;
    }

    /**
     * @return mixed
     */
    public function getMinTimeScrollAction()
    {
        return $this->minTimeScrollAction;
    }

    /**
     * @param mixed $minTimeScrollAction
     */
    public function setMinTimeScrollAction($minTimeScrollAction): void
    {
        $this->minTimeScrollAction = $minTimeScrollAction;
    }

    /**
     * @return mixed
     */
    public function getAvgTimeScrollAction()
    {
        return $this->avgTimeScrollAction;
    }

    /**
     * @param mixed $avgTimeScrollAction
     */
    public function setAvgTimeScrollAction($avgTimeScrollAction): void
    {
        $this->avgTimeScrollAction = $avgTimeScrollAction;
    }

    /**
     * @return mixed
     */
    public function getMaxTimeScrollAction()
    {
        return $this->maxTimeScrollAction;
    }

    /**
     * @param mixed $maxTimeScrollAction
     */
    public function setMaxTimeScrollAction($maxTimeScrollAction): void
    {
        $this->maxTimeScrollAction = $maxTimeScrollAction;
    }

    /**
     * @return mixed
     */
    public function getAmountUniqVisit()
    {
        return $this->amountUniqVisit;
    }

    /**
     * @param mixed $amountUniqVisit
     */
    public function setAmountUniqVisit($amountUniqVisit): void
    {
        $this->amountUniqVisit = $amountUniqVisit;
    }

    /**
     * @return mixed
     */
    public function getRateRejection()
    {
        return $this->rateRejection;
    }

    /**
     * @param mixed $rateRejection
     */
    public function setRateRejection($rateRejection): void
    {
        $this->rateRejection = $rateRejection;
    }

    /**
     * @return mixed
     */
    public function getAmountCalls()
    {
        return $this->amountCalls;
    }

    /**
     * @param mixed $amountCalls
     */
    public function setAmountCalls($amountCalls): void
    {
        $this->amountCalls = $amountCalls;
    }

    /**
     * @return mixed
     */
    public function getCalls()
    {
        return $this->calls;
    }

    /**
     * @param mixed $calls
     */
    public function setCalls($calls): void
    {
        $this->calls = $calls;
    }

    /**
     * @return mixed
     */
    public function getAmountBeginDialogs()
    {
        return $this->amountBeginDialogs;
    }

    /**
     * @param mixed $amountBeginDialogs
     */
    public function setAmountBeginDialogs($amountBeginDialogs): void
    {
        $this->amountBeginDialogs = $amountBeginDialogs;
    }

    /**
     * @return mixed
     */
    public function getAmountCompletedCalls()
    {
        return $this->amountCompletedCalls;
    }

    /**
     * @param mixed $amountCompletedCalls
     */
    public function setAmountCompletedCalls($amountCompletedCalls): void
    {
        $this->amountCompletedCalls = $amountCompletedCalls;
    }

    /**
     * @return mixed
     */
    public function getAmountNotCompletedCalls()
    {
        return $this->amountNotCompletedCalls;
    }

    /**
     * @param mixed $amountNotCompletedCalls
     */
    public function setAmountNotCompletedCalls($amountNotCompletedCalls): void
    {
        $this->amountNotCompletedCalls = $amountNotCompletedCalls;
    }

    /**
     * @return mixed
     */
    public function getDialogs()
    {
        return $this->dialogs;
    }

    /**
     * @param mixed $dialogs
     */
    public function setDialogs($dialogs): void
    {
        $this->dialogs = $dialogs;
    }

    /**
     * @return mixed
     */
    public function getInteractions()
    {
        return $this->interactions;
    }

    /**
     * @param mixed $interactions
     */
    public function setInteractions($interactions): void
    {
        $this->interactions = $interactions;
    }

}
